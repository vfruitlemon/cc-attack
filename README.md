## CC Attack
*DDoS a website using Socks proxies. For educational use only.*

Setup:
```
pip3 install -r requirements.txt
python3 cc.py
```

On Windows you may need to change `python3 cc.py` to `py cc.py`.
